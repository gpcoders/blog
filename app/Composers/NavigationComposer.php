<?php
namespace App\Composers;

use App\Post;
use Illuminate\View\View;
use App\Category;

class NavigationComposer {

    /**
     * @param View $view
     */
    public function compose(View $view) {

        $this->composeCategories($view);
        $this->composePopularPosts($view);
    }

    /**
     * Listing sidebar categories
     *
     * @param View $view
     */
    public function composeCategories(View $view) {
        $categories = Category::with(['posts' => function($query) {
                $query->published();
            }])->orderBy('title', 'asc')->get();

            $view->with('categories', $categories);
    }

    /**
     * Showing popular posts
     *
     * @param View $view
     */
    public function composePopularPosts(View $view) {

        $popularPosts = Post::published()->popular()->take(3)->get();
        $view->with('popularPosts', $popularPosts);

    }
}
