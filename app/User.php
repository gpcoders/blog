<?php

namespace App;

use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts() {
        return $this->hasMany(Post::class, 'author_id');
    }

    public function gravatar() {
        $email = $this->email;
        $default = "https://gpcoders.com/wp-content/uploads/2018/03/author.png";
        $size = 100;

        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;

    }

    public function getRouteKeyName() {
        return 'slug';
    }

    /**
     * @param $value
     * @return null
     */
    public function getBioHtmlAttribute($value) {
        return $this->bio ? Markdown::convertToHtml(e($this->bio)) : NULL;
    }
}
