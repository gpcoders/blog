<?php

namespace App;

use Carbon\Carbon;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author() {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category() {
        return $this->belongsTo(Category::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getImageUrlAttribute($value) {
        $imageUrl = "";
        if (! is_null($this->image)) {
            $imagePath = public_path() . "/img/" . $this->image;
            if ( file_exists( $imagePath ) ) $imageUrl = asset("img/" . $this->image);
        }
        return $imageUrl;
    }

    /**
     * @param $value
     * @return string
     */
    public function getThumbImageUrlAttribute($value) {
        $imageUrl = "";
        if (! is_null($this->image)) {
            $ext = substr(strrchr($this->image, '.'), 1);
            $thumbnail = str_replace(".{$ext}", "_thumb.{$ext}", $this->image);

            $imagePath = public_path() . "/img/" . $thumbnail;
            if ( file_exists( $imagePath ) ) $imageUrl = asset("img/" . $thumbnail);
        }
        return $imageUrl;
    }

    /**
     * @Attributes
     * @param $value
     * @return mixed
     */
    public function getDateAttribute($value) {
        return is_null($this->published_at) ? '' : $this->published_at->diffForHumans();
    }

    /**
     * @Scopes
     * @return mixed
     */
    public function scopeLatestFirst($query) {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * @return mixed
     */
    public function scopePublished($query) {
        return $query->where('published_at', "<=", "NOW()");
    }

    /**
     * @return mixed
     */
    public function scopePopular($query) {
        return $query->orderBy('view_count', "<=", "desc");
    }

    /**
     * @param $value
     * @return null
     */
    public function getBodyHtmlAttribute($value) {
        return $this->body ? Markdown::convertToHtml(e($this->body)) : NULL;
    }

    /**
     * @param $value
     * @return null
     */
    public function getExcerptHtmlAttribute($value) {
        return $this->excerpt ? Markdown::convertToHtml(e($this->excerpt)) : NULL;
    }
}
