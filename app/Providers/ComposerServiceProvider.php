<?php

namespace App\Providers;

use App\Composers\NavigationComposer;
use App\Post;
use Illuminate\Support\ServiceProvider;
use App\Category;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {

        view()->composer('layouts.sidebar', NavigationComposer::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {

    }
}
