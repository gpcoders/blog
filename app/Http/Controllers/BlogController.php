<?php

namespace App\Http\Controllers;


use App\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller {

    protected $limit = 3;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $categories = Category::with(['posts' => function($query) {
            $query->published();
        }])
            ->orderBy('title', 'asc')
            ->get();

            $posts = Post::with('author')
                ->latestFirst()
                ->published()
                ->simplePaginate($this->limit);
            return view("blog.index", compact('posts', 'categories'));
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post) {


        #method 1
        #$viewCount = $post->view_count + 1;
        #$post->update(['view_count' => $viewCount]);

        #method 2
        $post->increment('view_count');

        return view("blog.show", compact('post'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category(Category $category) {

        $categoryName = $category->title;

        $posts = $category->posts()
            ->with('author')
            ->latestFirst()
            ->published()
            ->simplePaginate($this->limit);

        return view("blog.index", compact('posts', 'categoryName'));

    }

    /**
     * @param User $author
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function author(User $author) {
        $authorName = $author->name;

        $posts = $author->posts()
            ->with('category')
            ->latestFirst()
            ->published()
            ->simplePaginate($this->limit);

        return view("blog.index", compact('posts', 'authorName'));
    }
}
