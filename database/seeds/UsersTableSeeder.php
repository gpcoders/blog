<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();

        $faker = \Faker\Factory::create();
        DB::table('users')->insert([
            [
                'name'  => 'Gagan Jaswal',
                'slug'  =>  'gagan-jaswal',
                'email' =>  'gaganjaswal9187@gmail.com',
                'password'  =>  bcrypt('secret'),
                'bio'       =>  $faker->text(rand(250, 300))
            ],
            [
                'name'  => 'Pardip Bhatti',
                'slug'  =>  'pardip-bhatti',
                'email' =>  'pardipbhatti8791@gmail.com',
                'password'  =>  bcrypt('secret'),
                'bio'       =>  $faker->text(rand(250, 300))
            ],
            [
                'name'  => 'Sam Bhatti',
                'slug'  =>  'sam-bhatti',
                'email' =>  'sam@gmail.com',
                'password'  =>  bcrypt('secret'),
                'bio'       =>  $faker->text(rand(250, 300))
            ],

        ]);
    }
}
